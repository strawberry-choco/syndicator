use crate::models::{
    sql::{ArticleCountCache, SubscriptionData},
    Article, Subscription,
};
use crossbeam_channel::Receiver;
use diesel::prelude::*;

type Sender<T> = crossbeam_channel::Sender<Result<T, String>>;
type SubscriptionUrl = String;
type ArticleId = String;

#[derive(Debug)]
pub enum DBCommand {
    FindAllSubscriptions(Sender<Vec<Subscription>>),
    FindAllArticlesForSubscription(Sender<Vec<Article>>, SubscriptionUrl),
    SaveSubscriptions(Sender<usize>, Vec<Subscription>),
    InitSubscriptions(Sender<usize>, Vec<crate::models::sql::Subscription>),
    DeleteSubscription(Sender<usize>, SubscriptionUrl),
    SaveArticles(Sender<usize>, Vec<Article>, SubscriptionUrl),
    UpdateArticleReadStatus(Sender<usize>, ArticleId, bool),
    UpdateAllArticleReadStatus(Sender<usize>, SubscriptionUrl, bool),
}

pub fn init(db_path: String, rx: Receiver<DBCommand>) -> std::thread::JoinHandle<()> {
    let conn = create_connection(&db_path).expect("Cannot create connection to database");

    std::thread::spawn(move || start_loop(conn, rx))
}

fn start_loop(conn: SqliteConnection, rx: Receiver<DBCommand>) {
    while let Ok(command) = rx.recv() {
        execute_command(&conn, command);
    }
}

fn execute_command(conn: &SqliteConnection, command: DBCommand) {
    match command {
        DBCommand::FindAllSubscriptions(tx) => {
            let result = find_all_subscriptions(&conn).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::FindAllArticlesForSubscription(tx, sub_url) => {
            let result =
                find_all_articles_for_subscription(&conn, &sub_url).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::SaveSubscriptions(tx, subs) => {
            let result = save_subscriptions(&conn, subs).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::InitSubscriptions(tx, subs) => {
            let result = init_subscriptions(&conn, &subs).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::DeleteSubscription(tx, sub) => {
            let result = delete_subscription(&conn, &sub).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::SaveArticles(tx, articles, sub_url) => {
            let result = save_articles(&conn, articles, &sub_url).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::UpdateArticleReadStatus(tx, article_id, is_read) => {
            let result =
                update_article_read_status(&conn, &article_id, is_read).map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
        DBCommand::UpdateAllArticleReadStatus(tx, subscription_url, is_read) => {
            let result = update_all_article_read_status(&conn, &subscription_url, is_read)
                .map_err(|e| e.to_string());
            let _ = tx.send(result);
        }
    }
}

fn create_connection(path: &str) -> ConnectionResult<SqliteConnection> {
    use diesel::connection::SimpleConnection;

    embed_migrations!("migrations");

    let conn = SqliteConnection::establish(path)?;

    embedded_migrations::run(&conn).expect("Cannot init database");
    conn.batch_execute("PRAGMA foreign_keys = on")
        .expect("Cannot enforce foreign key contraints");

    Ok(conn)
}

fn find_all_subscriptions(conn: &SqliteConnection) -> QueryResult<Vec<Subscription>> {
    use crate::schema::article_count_cache;
    use crate::schema::subscriptions;
    use crate::schema::subscriptions_data;

    let result = subscriptions::table
        .left_join(subscriptions_data::table.on(subscriptions::url.eq(subscriptions_data::url)))
        .left_join(article_count_cache::table.on(subscriptions::url.eq(article_count_cache::url)))
        .order(subscriptions::order.asc())
        .load::<(
            crate::models::sql::Subscription,
            Option<SubscriptionData>,
            Option<ArticleCountCache>,
        )>(conn)?;

    let result = result
        .into_iter()
        .map(|item| item.into())
        .collect::<Vec<Subscription>>();

    Ok(result)
}

fn find_all_articles_for_subscription(
    conn: &SqliteConnection,
    url: &str,
) -> QueryResult<Vec<Article>> {
    use crate::schema::articles;

    articles::table
        .filter(articles::subscription_data_id.eq(url))
        // TODO: enable this when we can deal with dates
        // .order(articles::date.desc())
        .load::<crate::models::sql::Article>(conn)
        .map(|a| a.into_iter().map(|a| a.into()).collect())
}

fn save_subscriptions(conn: &SqliteConnection, subs: Vec<Subscription>) -> QueryResult<usize> {
    use crate::schema::subscriptions_data;

    let subs_data = subs
        .into_iter()
        .map(|sub| sub.into())
        .collect::<Vec<SubscriptionData>>();

    diesel::replace_into(subscriptions_data::table)
        .values(&subs_data)
        .execute(conn)
}

fn init_subscriptions(
    conn: &SqliteConnection,
    subs: &[crate::models::sql::Subscription],
) -> QueryResult<usize> {
    use crate::schema::subscriptions;

    diesel::delete(subscriptions::table).execute(conn)?;
    diesel::insert_into(subscriptions::table)
        .values(subs)
        .execute(conn)
}

fn save_articles(
    conn: &SqliteConnection,
    articles: Vec<Article>,
    parent_url: &str,
) -> QueryResult<usize> {
    use crate::schema::articles;

    let articles = articles
        .into_iter()
        .map(|a| (a, parent_url.to_owned()).into())
        .collect::<Vec<crate::models::sql::Article>>();

    diesel::insert_or_ignore_into(articles::table)
        .values(&articles)
        .execute(conn)
}

fn delete_subscription(conn: &SqliteConnection, url: &str) -> QueryResult<usize> {
    use crate::schema::subscriptions;

    delete_articles(conn, url)?;

    diesel::delete(subscriptions::table)
        .filter(subscriptions::url.eq(url))
        .execute(conn)
}

fn delete_articles(conn: &SqliteConnection, url: &str) -> QueryResult<usize> {
    use crate::schema::articles;

    diesel::delete(articles::table)
        .filter(articles::subscription_data_id.eq(url))
        .execute(conn)
}

fn update_article_read_status(
    conn: &SqliteConnection,
    article_id: &str,
    is_read: bool,
) -> QueryResult<usize> {
    use crate::schema::articles;

    diesel::update(articles::table.filter(articles::id.eq(article_id)))
        .set(articles::is_read.eq(is_read))
        .execute(conn)
}

fn update_all_article_read_status(
    conn: &SqliteConnection,
    subscription_url: &str,
    is_read: bool,
) -> QueryResult<usize> {
    use crate::schema::articles;

    diesel::update(articles::table.filter(articles::subscription_data_id.eq(subscription_url)))
        .set(articles::is_read.eq(is_read))
        .execute(conn)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::prelude::*;

    #[test]
    fn integration() {
        let mut conn = create_connection(":memory:").unwrap();

        // test subscription insertion
        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        // test article insertion
        let article1 = create_article();
        let result = save_articles(&conn, vec![article1.clone()], &sub1.url);
        assert_eq!(result, Ok(1));

        // test invalid parent url insertion
        let article2 = create_article();
        let result = save_articles(&conn, vec![article2], "-");
        assert!(result.is_err());

        // test article reading
        let result = find_all_articles_for_subscription(&conn, &sub1.url).unwrap();
        assert_eq!(result.len(), 1);
        assert_eq!(result[0], article1);

        // test insert article twice
        let article3 = Article {
            id: article1.id.clone(),
            ..create_article()
        };
        save_articles(&mut conn, vec![article3.clone()], &sub1.url).unwrap();
        let result = find_all_articles_for_subscription(&conn, &sub1.url).unwrap();
        let result = result.iter().find(|a| a.id == article3.id).unwrap();
        assert_eq!(result, &article1);

        // test deletion
        let result = delete_subscription(&conn, &sub1.url);
        assert_eq!(result, Ok(1));

        let result = find_all_subscriptions(&conn).unwrap();
        let result = result.iter().find(|s| s.url == sub1.url);
        assert!(result.is_none());

        let result = find_all_articles_for_subscription(&conn, &sub1.url).unwrap();
        assert_eq!(result.len(), 0);
    }

    #[test]
    fn given_subscriptions_when_save_subscriptions_then_successfully_save() {
        let conn = create_connection(":memory:").unwrap();

        // test subscription insertion
        let mut sub1 = create_subscription();
        let mut sub2 = create_subscription();

        sub1.order = 0;
        sub2.order = 1;

        let result = init_subscriptions(
            &conn,
            &vec![to_sql_subscription(&sub1), to_sql_subscription(&sub2)],
        );
        assert_eq!(result, Ok(2));
        let result = save_subscriptions(&conn, vec![sub1.clone(), sub2.clone()]);
        assert_eq!(result, Ok(2));

        let result = find_all_subscriptions(&conn);
        assert_eq!(result, Ok(vec![sub1, sub2]));
    }

    #[test]
    fn given_subscriptions_when_init_subscriptions_then_always_overwrite() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        let sub2 = create_subscription();
        init_subscriptions(
            &conn,
            &vec![to_sql_subscription(&sub1), to_sql_subscription(&sub2)],
        )
        .unwrap();
        save_subscriptions(&conn, vec![sub1, sub2]).unwrap();

        let sub3 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub3)]).unwrap();
        save_subscriptions(&conn, vec![sub3.clone()]).unwrap();

        let result = find_all_subscriptions(&conn);
        assert_eq!(result, Ok(vec![sub3]));
    }

    #[test]
    fn given_no_article_when_reading_subscription_then_subscription_total_count_is_0() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let sub = find_subscription(&conn, &sub1.url);
        let total = sub.map(|sub| sub.total);
        assert_eq!(Some(0), total);
    }

    #[test]
    fn given_no_article_when_inserting_article_then_subscription_total_count_updates() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let article1 = create_article();
        save_articles(&conn, vec![article1.clone()], &sub1.url).unwrap();

        let sub = find_subscription(&conn, &sub1.url);
        let total = sub.map(|sub| sub.total);
        assert_eq!(Some(1), total);
    }

    #[test]
    fn given_no_article_when_reading_subscription_then_subscription_unread_count_is_0() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let sub = find_subscription(&conn, &sub1.url);
        let unread = sub.map(|sub| sub.unread);
        assert_eq!(Some(0), unread);
    }

    #[test]
    fn given_no_article_when_inserting_article_then_subscription_unread_count_updates() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let article1 = create_article();
        save_articles(&conn, vec![article1.clone()], &sub1.url).unwrap();

        let sub = find_subscription(&conn, &sub1.url);
        let unread = sub.map(|sub| sub.unread);
        assert_eq!(Some(1), unread);
    }

    #[test]
    fn given_article_when_updating_read_status_then_subscription_unread_count_updates() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let article1 = create_article();
        save_articles(&conn, vec![article1.clone()], &sub1.url).unwrap();

        let result = update_article_read_status(&conn, &article1.id, true);
        assert_eq!(result, Ok(1));

        let sub = find_subscription(&conn, &sub1.url);
        let unread = sub.map(|sub| sub.unread);
        assert_eq!(Some(0), unread);
    }

    #[test]
    fn given_only_unread_articles_when_updating_read_status_then_subscription_unread_count_updates()
    {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        init_subscriptions(&conn, &vec![to_sql_subscription(&sub1)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let article1 = create_article();
        let article2 = create_article();
        let article3 = create_article();
        save_articles(
            &conn,
            vec![article1.clone(), article2.clone(), article3.clone()],
            &sub1.url,
        )
        .unwrap();

        let result = update_all_article_read_status(&conn, &sub1.url, true);
        assert_eq!(result, Ok(3));

        let articles = find_all_articles_for_subscription(&conn, &sub1.url).unwrap();
        let read_count = articles.iter().filter(|article| article.is_read).count();
        assert_eq!(read_count, 3);

        let result = update_all_article_read_status(&conn, &sub1.url, false);
        assert_eq!(result, Ok(3));

        let articles = find_all_articles_for_subscription(&conn, &sub1.url).unwrap();
        let read_count = articles.iter().filter(|article| article.is_read).count();
        assert_eq!(read_count, 0);
    }

    #[test]
    fn given_no_custom_title_when_reading_subscriptions_then_use_title_from_subscription() {
        let conn = create_connection(":memory:").unwrap();

        let sub1 = create_subscription();
        let sub_without_custom_title = Subscription {
            title: None,
            ..sub1.clone()
        };
        init_subscriptions(&conn, &[to_sql_subscription(&sub_without_custom_title)]).unwrap();
        save_subscriptions(&conn, vec![sub1.clone()]).unwrap();

        let result = find_all_subscriptions(&conn);
        assert_eq!(Ok(vec![sub1]), result);
    }

    #[test]
    fn given_custom_title_when_reading_subscriptions_then_use_custom_title() {
        let conn = create_connection(":memory:").unwrap();

        let sub_with_custom_title = create_subscription();
        init_subscriptions(&conn, &[to_sql_subscription(&sub_with_custom_title)]).unwrap();
        save_subscriptions(&conn, vec![sub_with_custom_title.clone()]).unwrap();

        let result = find_all_subscriptions(&conn);
        assert_eq!(Ok(vec![sub_with_custom_title]), result);
    }

    fn find_subscription(conn: &SqliteConnection, sub_url: &str) -> Option<Subscription> {
        let all_subs = find_all_subscriptions(&conn).unwrap();
        all_subs.into_iter().find(|sub| sub.url == sub_url)
    }

    fn create_subscription() -> Subscription {
        Subscription {
            title: Some(rand_string(64)),
            description: Some(rand_string(64)),
            url: rand_string(64),
            order: rand::random::<i32>(),
            proxy: None,
            unread: 0,
            total: 0,
        }
    }

    fn create_article() -> Article {
        Article {
            id: rand_string(64),
            title: rand_string(64),
            url: rand_string(64),
            author: rand_string(64),
            content: rand_string(64),
            date: rand_string(64),
            is_read: false,
        }
    }

    fn to_sql_subscription(sub: &Subscription) -> crate::models::sql::Subscription {
        crate::models::sql::Subscription {
            url: sub.url.clone(),
            order: sub.order.clone(),
            proxy: sub.proxy.clone(),
            title: sub.title.clone(),
        }
    }

    fn rand_string(length: usize) -> String {
        let mut rng = rand::thread_rng();

        std::iter::repeat(())
            .map(|_| rng.sample(rand::distributions::Alphanumeric))
            .take(length)
            .collect()
    }
}
