use crate::db::DBCommand;
use crate::{
    models::{Article, CustomFeed, Subscription},
    settings::FeedItem,
    ui::views::subscription_list::reload_subscriptions,
    util::{request, RequestError},
};
use crossbeam_channel::{Receiver, Sender};
use cursive::Cursive;
use std::thread::JoinHandle;
use ureq::Proxy;

#[derive(Debug)]
struct NetworkError;

impl std::fmt::Display for NetworkError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Nework error occoured.")
    }
}

impl std::error::Error for NetworkError {}

type XSender = Sender<Box<dyn FnOnce(&mut Cursive) + 'static + Send>>;
pub enum NetworkCommand {
    FetchSubscriptions(XSender, Vec<FeedItem>),
}

pub fn init(db_tx: Sender<DBCommand>, rx: Receiver<NetworkCommand>) -> JoinHandle<()> {
    std::thread::spawn(move || start_event_loop(db_tx, rx))
}

fn start_event_loop(db_tx: Sender<DBCommand>, rx: Receiver<NetworkCommand>) {
    while let Ok(message) = rx.recv() {
        process_message(&db_tx, message);
    }
}

fn process_message(db_tx: &Sender<DBCommand>, message: NetworkCommand) {
    match message {
        NetworkCommand::FetchSubscriptions(tx, feeds) => {
            let pool = threadpool::Builder::new().build();
            feeds.into_iter().for_each(|feed| {
                let db_tx = db_tx.clone();
                let tx = tx.clone();
                pool.execute(move || load_feed(feed, db_tx, tx));
            });
        }
    }
}

fn load_feed(feed: FeedItem, db_tx: Sender<DBCommand>, tx: XSender) {
    let (subscription, articles) = match fetch_feed(&feed) {
        Ok(a) => a,
        Err(err) => {
            log::error!("Something went wrong downloading feed: {}", err);
            return;
        }
    };
    let result = request(&db_tx, |tx| {
        DBCommand::SaveSubscriptions(tx, vec![subscription.clone()])
    });
    let _ = match result {
        Ok(v) => v.unwrap(),
        Err(RequestError::RecvError(_)) | Err(RequestError::SendError(_)) => return,
    };

    let articles = articles.into_iter().rev().collect();
    let result = request(&db_tx, |tx| {
        DBCommand::SaveArticles(tx, articles, subscription.url.clone())
    });
    let _ = match result {
        Ok(v) => v.unwrap(),
        Err(RequestError::RecvError(_)) | Err(RequestError::SendError(_)) => return,
    };

    let result = request(&db_tx, DBCommand::FindAllSubscriptions);
    if let Ok(Ok(_)) = result {
        let _ = tx.send(Box::new(reload_subscriptions(db_tx)));
    }
}

fn fetch_feed(
    feed_item: &FeedItem,
) -> Result<(Subscription, Vec<Article>), Box<dyn std::error::Error>> {
    let response = fetch_url(&feed_item)?;
    let feed = feed_rs::parser::parse(response.as_bytes())?;

    let (sub, articles) = CustomFeed(feed).into();

    let sub = Subscription {
        // workaround for shitty rss extension <atom:link>
        url: feed_item.url.clone(),
        proxy: feed_item.proxy.clone(),
        ..sub
    };

    Ok((sub, articles))
}

fn fetch_url(feed: &FeedItem) -> Result<String, Box<dyn std::error::Error>> {
    let mut response = ureq::get(&feed.url);
    if feed.proxy.is_some() {
        let proxy = Proxy::new(feed.proxy.clone().unwrap());
        response.set_proxy(proxy?);
    }
    let response = response.call();
    if response.error() {
        Err(NetworkError.into())
    } else {
        Ok(response.into_string()?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::mock;

    #[test]
    fn fetch_url_test() {
        let mock = mock("GET", "/atom")
            .with_status(200)
            .with_header("content-type", "application/atom+xml")
            .with_body_from_file("tests/files/feed.atom")
            .create();

        let url = format!("{}/atom", mockito::server_url());
        let actual = fetch_url(&FeedItem {
            url,
            proxy: None,
            title: None,
        })
        .unwrap();
        let expected = std::fs::read_to_string("tests/files/feed.atom").unwrap();

        assert_eq!(actual, expected);
        mock.assert();
    }
}
