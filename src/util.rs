use crossbeam_channel::{RecvError, SendError, Sender};
use std::fmt::{Debug, Display};

#[derive(Debug)]
pub enum RequestError<T: Debug> {
    SendError(SendError<T>),
    RecvError(RecvError),
}

impl<T: std::fmt::Debug> From<RecvError> for RequestError<T> {
    fn from(v: RecvError) -> Self {
        RequestError::RecvError(v)
    }
}

impl<T: Debug> From<SendError<T>> for RequestError<T> {
    fn from(v: SendError<T>) -> Self {
        RequestError::SendError(v)
    }
}

impl<T: Debug> Display for RequestError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            RequestError::SendError(v) => Display::fmt(v, f),
            RequestError::RecvError(v) => Display::fmt(v, f),
        }
    }
}

impl<T: Debug + Send + 'static> std::error::Error for RequestError<T> {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            RequestError::SendError(v) => Some(v),
            RequestError::RecvError(v) => Some(v),
        }
    }
}

pub fn request<T, V, F>(tx: &Sender<T>, call: F) -> Result<V, RequestError<T>>
where
    T: std::fmt::Debug + std::marker::Send,
    F: FnOnce(Sender<V>) -> T,
{
    let (tx1, rx) = crossbeam_channel::unbounded::<V>();
    tx.send(call(tx1))?;

    Ok(rx.recv()?)
}

pub fn log10(n: usize) -> usize {
    if n < 10 {
        0
    } else {
        1 + log10(n / 10)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn log10_test() {
        assert_eq!(2, log10(100));
        assert_eq!(9, log10(4857392075));
    }
}
