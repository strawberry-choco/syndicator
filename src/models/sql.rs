use crate::schema::article_count_cache;
use crate::schema::articles;
use crate::schema::subscriptions;
use crate::schema::subscriptions_data;
use diesel::Identifiable;

#[derive(Debug, PartialEq, Eq, Clone, Queryable, Insertable, Identifiable)]
#[primary_key(url)]
#[table_name = "subscriptions_data"]
pub struct SubscriptionData {
    pub url: String,
    pub title: Option<String>,
    pub description: Option<String>,
}

#[derive(Debug, PartialEq, Eq, Clone, Queryable, Insertable, Identifiable)]
#[primary_key(url)]
#[table_name = "subscriptions"]
pub struct Subscription {
    pub url: String,
    pub order: i32,
    pub proxy: Option<String>,
    pub title: Option<String>,
}

#[derive(Debug, PartialEq, Eq, Clone, Queryable, Insertable, Identifiable)]
#[primary_key(url)]
#[table_name = "article_count_cache"]
pub struct ArticleCountCache {
    pub url: String,
    pub total: i32,
    pub unread: i32,
}

#[derive(Debug, PartialEq, Eq, Clone, Queryable, Insertable, Associations)]
#[belongs_to(parent = "SubscriptionData")]
pub struct Article {
    pub id: String,
    pub title: String,
    pub url: String,
    pub author: String,
    pub content: String,
    pub date: String,
    pub is_read: bool,
    pub subscription_data_id: String,
}
