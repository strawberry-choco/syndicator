use sql::ArticleCountCache;

pub mod sql;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Subscription {
    pub url: String,
    pub title: Option<String>,
    pub description: Option<String>,
    pub order: i32,
    pub proxy: Option<String>,
    pub unread: i32,
    pub total: i32,
}

type JoinData = (
    sql::Subscription,
    Option<sql::SubscriptionData>,
    Option<ArticleCountCache>,
);
impl From<JoinData> for Subscription {
    fn from((sub, sub_data, article_count): JoinData) -> Self {
        Self {
            url: sub.url,
            title: sub
                .title
                .or_else(|| sub_data.clone().and_then(|sub_data| sub_data.title)),
            description: sub_data.and_then(|sub_data| sub_data.description),
            order: sub.order,
            proxy: sub.proxy,
            unread: article_count
                .as_ref()
                .map(|count| count.unread)
                .unwrap_or(0),
            total: article_count.map(|count| count.total).unwrap_or(0),
        }
    }
}

impl From<Subscription> for sql::SubscriptionData {
    fn from(subscription: Subscription) -> Self {
        Self {
            url: subscription.url,
            title: subscription.title,
            description: subscription.description,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Article {
    pub id: String,
    pub title: String,
    pub url: String,
    pub author: String,
    pub content: String,
    pub date: String,
    pub is_read: bool,
}

impl From<sql::Article> for Article {
    fn from(article_sql: sql::Article) -> Self {
        Self {
            id: article_sql.id,
            title: article_sql.title,
            url: article_sql.url,
            author: article_sql.author,
            content: article_sql.content,
            date: article_sql.date,
            is_read: article_sql.is_read,
        }
    }
}

impl From<(Article, String)> for sql::Article {
    fn from((article, parent): (Article, String)) -> Self {
        Self {
            id: article.id,
            title: article.title,
            url: article.url,
            author: article.author,
            content: article.content,
            date: article.date,
            is_read: article.is_read,
            subscription_data_id: parent,
        }
    }
}

pub struct CustomFeed(pub feed_rs::model::Feed);

impl From<CustomFeed> for (Subscription, Vec<Article>) {
    fn from(f: CustomFeed) -> Self {
        let subscription = Subscription {
            url: String::new(),
            title: f.0.title.map(|t| t.content),
            description: f.0.description.map(|d| d.content),
            order: 0,
            proxy: None,
            unread: 0,
            total: 0,
        };

        let articles = f.0.entries.into_iter().map(Into::into).collect();

        (subscription, articles)
    }
}

impl From<feed_rs::model::Entry> for Article {
    fn from(e: feed_rs::model::Entry) -> Self {
        let title = e.title.map(|t| t.content).unwrap_or_default();
        let title = title.trim().to_owned();
        let author = e
            .authors
            .into_iter()
            .next()
            .map(|p| p.name)
            .unwrap_or_default();
        let content1 = e.content.and_then(|c| c.body).unwrap_or_default();
        let content2 = e.summary.map(|s| s.content).unwrap_or_default();
        let content = match content1.len() >= content2.len() {
            true => content1,
            false => content2,
        };
        let date = e
            .published
            .map(|date| date.to_rfc3339())
            .unwrap_or_default();
        let url = e
            .links
            .into_iter()
            .next()
            .map(|l| l.href)
            .unwrap_or_default();

        Self {
            id: e.id,
            title,
            url,
            author,
            content,
            date,
            is_read: false,
        }
    }
}
