use crate::models::Subscription;
use opml::{Head, OPML};
use std::{
    fs::File,
    io::{ErrorKind, Write},
    path::PathBuf,
};

#[derive(Debug)]
pub struct FileCountOverflow;

impl std::error::Error for FileCountOverflow {}
impl std::fmt::Display for FileCountOverflow {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Count exceeded maximum allowed number")
    }
}

pub fn convert_subscriptions_to_opml(subscriptions: Vec<Subscription>) -> Result<String, String> {
    let mut opml = OPML::default();
    opml.head = Some(Head {
        title: Some("Syndicator Export".to_string()),
        ..Head::default()
    });

    subscriptions.into_iter().for_each(|subscription| {
        let url = subscription.url;
        let title = subscription.title;
        let title = title.unwrap_or_else(|| url.clone());

        opml.add_feed(&title, &url);
    });

    opml.to_xml()
}

pub fn export_opml(opml: String) -> Result<(), Box<dyn std::error::Error>> {
    let user_dir = directories_next::UserDirs::new().unwrap();
    let mut download_path = user_dir.download_dir().unwrap().to_path_buf();
    download_path.push("export.opml");

    let mut file = get_file(download_path, 0)?;
    file.write_all(opml.as_bytes())?;

    Ok(())
}

fn get_file(mut path: PathBuf, count: usize) -> Result<File, Box<dyn std::error::Error>> {
    let name = match count {
        0 => "export".to_string(),
        v => format!("export ({})", v),
    };
    path.set_file_name(name);
    path.set_extension("opml");

    let file = std::fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(path.clone());

    match file {
        Ok(file) => Ok(file),
        Err(err) if err.kind() == ErrorKind::AlreadyExists && count == usize::MAX => {
            Err(FileCountOverflow.into())
        }
        Err(err) if err.kind() == ErrorKind::AlreadyExists => get_file(path, count + 1),
        Err(err) => Err(err.into()),
    }
}
