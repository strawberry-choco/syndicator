use cursive::{
    theme::{Color, ColorStyle, ColorType, Effect, PaletteColor, Style},
    utils::span::SpannedString,
};
use enumset::EnumSet;
use html2text::render::text_renderer::{
    RichAnnotation, TaggedLine, TaggedLineElement, TaggedString,
};

use crate::models::Article;

type Line = TaggedLine<Vec<RichAnnotation>>;
type Element = TaggedLineElement<Vec<RichAnnotation>>;
type Content = TaggedString<Vec<RichAnnotation>>;

struct ElementOutput {
    style: Style,
    links: Vec<String>,
    content: String,
}

struct TagOutput {
    effects: EnumSet<Effect>,
    color: Option<ColorStyle>,
    link: Option<String>,
}

impl TagOutput {
    fn with_effect(effect: Effect) -> Self {
        TagOutput {
            effects: EnumSet::only(effect),
            color: None,
            link: None,
        }
    }
}

pub fn parse(article: &Article) -> SpannedString<Style> {
    let parse_result = html2text::from_read_rich(article.content.as_bytes(), usize::MAX);

    let (mut span, links) = parse_result
        .iter()
        .flat_map(line_to_element_outputs_with_line_break)
        .fold((SpannedString::new(), vec![]), |mut acc, output| {
            log::warn!("{:?}", output.content);
            acc.0.append_styled(output.content, output.style);
            for link in output.links.into_iter() {
                acc.1.push(link);
                acc.0.append_plain(format!("[{}]", acc.1.len()));
            }

            acc
        });

    span.append_plain("\n\n");
    let links = links
        .into_iter()
        .enumerate()
        .map(|(index, link)| format!("[{}] {}\n", index + 1, link))
        .collect::<String>();
    span.append_plain(links);

    span
}

fn line_to_element_outputs_with_line_break(line: &Line) -> Vec<ElementOutput> {
    let mut outputs = line_to_element_outputs(line);
    outputs.push(ElementOutput {
        style: Style::none(),
        links: vec![],
        content: String::from("\n"),
    });
    outputs
}

fn line_to_element_outputs(line: &Line) -> Vec<ElementOutput> {
    line.iter().filter_map(line_to_element_output).collect()
}

fn line_to_element_output(element: &Element) -> Option<ElementOutput> {
    match element {
        TaggedLineElement::Str(content) => Some(content_to_element_output(content)),
        TaggedLineElement::FragmentStart(_) => None,
    }
}

fn content_to_element_output(content: &Content) -> ElementOutput {
    let (links, style) = content
        .tag
        .iter()
        .filter_map(convert_tag_to_processable_output)
        .fold((vec![], Style::none()), |mut acc, output| {
            if let Some(link) = output.link {
                acc.0.push(link);
            }
            acc.1.effects.insert_all(output.effects);
            if let Some(color) = output.color {
                acc.1.color = Some(color);
            }

            acc
        });

    ElementOutput {
        style,
        links,
        content: content.s.to_owned(),
    }
}

fn convert_tag_to_processable_output(tag: &RichAnnotation) -> Option<TagOutput> {
    match tag {
        RichAnnotation::Default => None,
        RichAnnotation::Link(link) => Some(TagOutput {
            effects: EnumSet::only(Effect::Underline),
            color: Some(ColorStyle::new(
                ColorType::Color(Color::Rgb(51, 102, 204)),
                ColorType::Palette(PaletteColor::Background),
            )),
            link: Some(link.to_owned()),
        }),
        RichAnnotation::Image => None,
        RichAnnotation::Emphasis => Some(TagOutput::with_effect(Effect::Italic)),
        RichAnnotation::Strong => Some(TagOutput::with_effect(Effect::Bold)),
        RichAnnotation::Code => None,
        RichAnnotation::Preformat(_) => None,
        RichAnnotation::Strikeout => Some(TagOutput::with_effect(Effect::Strikethrough)),
    }
}
