use super::{stack::find_stack, subscription_list::reload_subscriptions, VIEW_SIZE};
use crate::{db::DBCommand, models::Article, util::request};
use crossbeam_channel::Sender;
use cursive::{
    traits::{Boxable, Nameable},
    views::{OnEventView, Panel, ScrollView, SelectView, ViewRef},
    Cursive,
};

const ARTICLE_LIST_VIEW_ID: &str = "article_list";
const ARTICLE_LIST_SCROLL_VIEW_ID: &str = "article_list_scroll";

pub fn init(siv: &mut Cursive, url: &str, db_tx: Sender<DBCommand>) {
    let view = SelectView::<Article>::new()
        .on_submit(on_submit)
        .with_name(ARTICLE_LIST_VIEW_ID)
        .fixed_size(VIEW_SIZE);

    let view = OnEventView::new(view)
        .on_event('r', on_read(db_tx.clone(), url.to_string()))
        .on_event('R', on_read_all(db_tx.clone(), url.to_string()))
        .on_event('u', on_unread(db_tx.clone(), url.to_string()))
        .on_event('U', on_unread_all(db_tx.clone(), url.to_string()))
        .on_event('g', jump_to_start)
        .on_event('G', jump_to_end);

    let view = ScrollView::new(view).with_name(ARTICLE_LIST_SCROLL_VIEW_ID);
    let view = Panel::new(view);

    if let Some(mut stack) = find_stack(siv) {
        stack.add_layer(view);
    }

    reload_articles(&db_tx, url)(siv);
}

fn on_submit(siv: &mut Cursive, article: &Article) {
    super::article::init(siv, article);
}

fn on_read(db_tx: Sender<DBCommand>, url: String) -> impl Fn(&mut Cursive) {
    move |s| {
        let db_tx = db_tx.clone();
        let id = match find_current_article_id(s) {
            Some(id) => id,
            None => return,
        };

        let result = request(&db_tx, |tx| {
            DBCommand::UpdateArticleReadStatus(tx, id, true)
        });

        if let Ok(Err(err)) = result {
            log::error!("{}", err);
        }

        reload_articles(&db_tx, &url)(s);
        reload_subscriptions(db_tx)(s);
    }
}

fn on_read_all(db_tx: Sender<DBCommand>, url: String) -> impl Fn(&mut Cursive) {
    move |s| {
        let db_tx = db_tx.clone();

        let result = request(&db_tx, |tx| {
            DBCommand::UpdateAllArticleReadStatus(tx, url.clone(), true)
        });

        if let Ok(Err(err)) = result {
            log::error!("{}", err);
        }

        reload_articles(&db_tx, &url)(s);
        reload_subscriptions(db_tx)(s);
    }
}

fn on_unread(db_tx: Sender<DBCommand>, url: String) -> impl Fn(&mut Cursive) {
    move |s| {
        let db_tx = db_tx.clone();

        let id = match find_current_article_id(s) {
            Some(id) => id,
            None => return,
        };

        let result = request(&db_tx, |tx| {
            DBCommand::UpdateArticleReadStatus(tx, id, false)
        });

        if let Ok(Err(err)) = result {
            log::error!("{}", err);
        }

        reload_articles(&db_tx, &url)(s);
        reload_subscriptions(db_tx)(s);
    }
}

fn on_unread_all(db_tx: Sender<DBCommand>, url: String) -> impl Fn(&mut Cursive) {
    move |s| {
        let db_tx = db_tx.clone();

        let result = request(&db_tx, |tx| {
            DBCommand::UpdateAllArticleReadStatus(tx, url.clone(), false)
        });

        if let Ok(Err(err)) = result {
            log::error!("{}", err);
        }

        reload_articles(&db_tx, &url)(s);
        reload_subscriptions(db_tx)(s);
    }
}

fn jump_to_start(s: &mut Cursive) {
    if let Some(mut view) = find_article_list(s) {
        view.set_selection(0);
    }
    if let Some(mut view) = find_article_list_scroll(s) {
        view.scroll_to_important_area();
    }
}

fn jump_to_end(s: &mut Cursive) {
    if let Some(mut view) = find_article_list(s) {
        let i = view.len();

        view.set_selection(i - 1);
    }
    if let Some(mut view) = find_article_list_scroll(s) {
        view.scroll_to_important_area();
    }
}

fn find_article_list(s: &mut Cursive) -> Option<ViewRef<SelectView<Article>>> {
    s.find_name(ARTICLE_LIST_VIEW_ID)
}

fn find_article_list_scroll(s: &mut Cursive) -> Option<ViewRef<ScrollView<SelectView<Article>>>> {
    s.find_name(ARTICLE_LIST_SCROLL_VIEW_ID)
}

fn find_current_article_id(s: &mut Cursive) -> Option<String> {
    find_article_list(s)
        .and_then(|view| view.selection())
        .map(|selection| selection.id.clone())
}

fn reload_articles(db_tx: &Sender<DBCommand>, url: &str) -> impl Fn(&mut Cursive) {
    let db_tx = db_tx.clone();
    let url = url.to_string();
    move |s| {
        let mut view = match find_article_list(s) {
            Some(v) => v,
            None => return,
        };

        let result = request(&db_tx, |tx| {
            DBCommand::FindAllArticlesForSubscription(tx, url.to_string())
        });

        let articles = match result {
            Ok(Ok(articles)) => articles,
            _ => return,
        };
        let articles = articles.into_iter().rev().collect();

        let selection = view.selection();
        view.clear();
        view.add_all(articles_to_items(articles));

        if let Some(selection) = selection {
            let i = view
                .iter()
                .map(|item| item.1)
                .position(|item| item.id == selection.id);

            if let Some(i) = i {
                view.set_selection(i);
            }
        }
    }
}

fn articles_to_items(articles: Vec<Article>) -> impl IntoIterator<Item = (String, Article)> {
    let reserved_space_for_count = crate::util::log10(articles.len()) + 1;

    articles
        .into_iter()
        .enumerate()
        .map(move |(index, article)| {
            let is_read_sign = if article.is_read { ' ' } else { 'U' };
            let text = format!(
                "{1:>0$} {2} {3}",
                reserved_space_for_count,
                index + 1,
                is_read_sign,
                article.title
            );

            (text, article)
        })
}
