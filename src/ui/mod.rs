use crate::{
    db::DBCommand,
    network::NetworkCommand,
    opml::{convert_subscriptions_to_opml, export_opml},
    util::{request, RequestError},
};
use crossbeam_channel::Sender;
use cursive::theme::{BaseColor, BorderStyle, Color, Palette, PaletteColor, Theme};

mod parser;
pub mod views;

pub struct UserData {
    pub db_tx: Sender<DBCommand>,
    pub network_tx: Sender<NetworkCommand>,
}

pub fn init(data: UserData) {
    let mut siv = cursive::default();

    siv.set_theme(default_theme());
    siv.add_global_callback('l', |s| s.toggle_debug_console());
    siv.add_global_callback('e', {
        let db_tx = data.db_tx.clone();
        move |_| export(db_tx.clone())
    });

    views::stack::init(&mut siv);
    views::subscription_list::init(&mut siv, data.db_tx, data.network_tx);

    siv.run();
}

fn export(db_tx: Sender<DBCommand>) {
    let subs = request(&db_tx, DBCommand::FindAllSubscriptions);
    let subs = match subs {
        Ok(v) => v.unwrap(),
        Err(RequestError::RecvError(_)) | Err(RequestError::SendError(_)) => return,
    };
    let xml = convert_subscriptions_to_opml(subs);
    export_opml(xml.unwrap()).unwrap();
}

fn default_theme() -> Theme {
    let mut palette = Palette::default();

    palette[PaletteColor::Background] = Color::Dark(BaseColor::Black);
    palette[PaletteColor::View] = Color::TerminalDefault;
    palette[PaletteColor::Primary] = Color::TerminalDefault;
    palette[PaletteColor::Highlight] = Color::Dark(BaseColor::Blue);

    Theme {
        shadow: false,
        borders: BorderStyle::Simple,
        palette,
    }
}
