<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/" xml:lang="en-US">
  <id>tag:github.com,2008:https://github.com/lupoDharkael/flameshot/releases</id>
  <link type="text/html" rel="alternate" href="https://github.com/lupoDharkael/flameshot/releases"/>
  <link type="application/atom+xml" rel="self" href="https://github.com/lupoDharkael/flameshot/releases.atom"/>
  <title>Release notes from flameshot</title>
  <updated>2018-08-17T12:17:46Z</updated>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.6.0</id>
    <updated>2018-08-17T12:20:41Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.6.0"/>
    <title>v0.6.0</title>
    <content type="html">&lt;h2&gt;Features&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Allow systray customization with themes. Use &quot;flameshot-tray&quot; as the name of the icon.&lt;/li&gt;
&lt;li&gt;Unification of the desktop file with actions.&lt;/li&gt;
&lt;li&gt;Notification when screenshots are saved in the clipboard.&lt;/li&gt;
&lt;li&gt;Use datetime as default name for pics.&lt;/li&gt;
&lt;li&gt;Undo/Redo with Ctrl+z and Ctrl+Shift+z.&lt;/li&gt;
&lt;li&gt;Add &quot;Take Screenshot&quot; option as menu item in the systray.&lt;/li&gt;
&lt;li&gt;Add Side-Panel (open it with Space).&lt;/li&gt;
&lt;li&gt;Add autostart to config flags.&lt;/li&gt;
&lt;li&gt;Add Pin tool.&lt;/li&gt;
&lt;li&gt;Filename: replace colons with dashes.&lt;/li&gt;
&lt;li&gt;Add Text tool.&lt;/li&gt;
&lt;li&gt;Delete Imgur image button after uploading it from the preview window.&lt;/li&gt;
&lt;li&gt;Capture single screen:&lt;br&gt;
flameshot screen (capture the screen containing the mouse) and&lt;br&gt;
flameshot screen -n 1 (capture the first screen).&lt;/li&gt;
&lt;li&gt;Store settings colors in hexadecimal format.&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Fixes&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;flameshot full -c shouldn&#39;t block the desktop.&lt;/li&gt;
&lt;li&gt;Now you can overwrite exported configuration with the same name as a previous exports.&lt;/li&gt;
&lt;li&gt;Fix flameshot --raw wait time with delay.&lt;/li&gt;
&lt;li&gt;Fix negative selection geometry bug.&lt;/li&gt;
&lt;li&gt;Improved hidpi support with some bugs fixed.&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.5.1</id>
    <updated>2018-05-15T10:10:25Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.5.1"/>
    <title>v0.5.1</title>
    <content type="html">&lt;h2&gt;Features&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Polish, French, Georgian, Chinese, Turkish and Russian translations&lt;/li&gt;
&lt;li&gt;Modal widgets doesn&#39;t prevent the start of a new capture&lt;/li&gt;
&lt;li&gt;Improved hidpi support (still needs some work)&lt;/li&gt;
&lt;li&gt;Tool buttons now don&#39;t go out of the screen&lt;/li&gt;
&lt;li&gt;Use of native file dialog&lt;/li&gt;
&lt;li&gt;Configurable opacity of the dark area outside the selection&lt;/li&gt;
&lt;li&gt;Autostart app as a configuration option&lt;/li&gt;
&lt;li&gt;Minor fixes&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.5.0</id>
    <updated>2017-12-20T15:05:37Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.5.0"/>
    <title>v0.5.0</title>
    <content type="html">&lt;h2&gt;Features&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Catalan translation.&lt;/li&gt;
&lt;li&gt;Debian package configuration.&lt;/li&gt;
&lt;li&gt;Add --raw flag: prints the raw bytes of the png after the capture.&lt;/li&gt;
&lt;li&gt;Bash completion.&lt;/li&gt;
&lt;li&gt;Blur tool.&lt;/li&gt;
&lt;li&gt;Preview draw size on mouse pointer after tool selection.&lt;/li&gt;
&lt;li&gt;App Launcher tool: choose an app to open the capture.&lt;/li&gt;
&lt;li&gt;Travis integration&lt;/li&gt;
&lt;li&gt;Configuration import, export and reset.&lt;/li&gt;
&lt;li&gt;Experimental Wayland support (Plasma &amp;amp; Gnome)&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Accesibility&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Capture selection resizable using any border.&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Fixes&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;App version shown properly&lt;/li&gt;
&lt;li&gt;Now the cli wont break if you preppend a dash before gui, full and config arguments.&lt;/li&gt;
&lt;li&gt;Fix rare crash condition creating a selection during a graphical capture.&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.4.2</id>
    <updated>2017-09-17T15:55:54Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.4.2"/>
    <title>v0.4.2</title>
    <content type="html">&lt;p&gt;&lt;strong&gt;hotfix&lt;/strong&gt;: persistent configuration wasn&#39;t handled correctly for new users, failing to set a &quot;initiated&quot; status flag in the configuration. That is used to let the program know if a new process of Flameshot is the first launch of the program.&lt;/p&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.4.1</id>
    <updated>2017-08-28T17:58:10Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.4.1"/>
    <title>v0.4.1</title>
    <content type="html">&lt;ul&gt;
&lt;li&gt;Slightly darker capture background (Cross cursor more visible in Plasma)&lt;/li&gt;
&lt;li&gt;Flameshot compiles with QT 5.3 (Debian 8, Linux Mint, etc)&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.4.0</id>
    <updated>2017-08-17T14:33:47Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.4.0"/>
    <title>v0.4.0</title>
    <content type="html">&lt;h2&gt;Features&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;The buttons hide when you have selected the whole screen and you try to draw behind them.&lt;/li&gt;
&lt;li&gt;Add informative labels in the filename editor.&lt;/li&gt;
&lt;li&gt;A better CLI parser with a more effective (and informative) syntax error handling.&lt;/li&gt;
&lt;li&gt;new CLI argument &#39;config&#39; to manage some configurations from terminal.&lt;/li&gt;
&lt;li&gt;More uniform button&#39;s hovering color.&lt;/li&gt;
&lt;li&gt;Automatic horizontal adjustment in Marker and Line tools.&lt;/li&gt;
&lt;li&gt;Enhanced menus with expandable layouts.&lt;/li&gt;
&lt;li&gt;Add graphical option to disable the trayicon.&lt;/li&gt;
&lt;li&gt;Now you can check the version of Flameshot!&lt;/li&gt;
&lt;li&gt;Shrink the selection area when pushing against the borders of the screen.&lt;/li&gt;
&lt;li&gt;Now you can change the thickness of the drawing tools using the mouse wheel!&lt;/li&gt;
&lt;li&gt;Some more little changes.&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Fixes&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Fix button&#39;s emerge animation while hovering.&lt;/li&gt;
&lt;li&gt;Fix freeze problem with save dialog and clipboard copy at the same time.&lt;/li&gt;
&lt;li&gt;Minor Fixes&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Extras&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;More code cleanup! (still more to do)&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.3.0</id>
    <updated>2017-07-25T10:52:19Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.3.0"/>
    <title>v0.3.0</title>
    <content type="html">&lt;h2&gt;Features&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Add Desktop entry to init flameshot without commands.&lt;/li&gt;
&lt;li&gt;New configuration menu design.&lt;/li&gt;
&lt;li&gt;Define custom names for your captures!&lt;/li&gt;
&lt;li&gt;Improved error notifications when saving captures.&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Fixes&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Multiple monitor support.&lt;/li&gt;
&lt;li&gt;Makefile flag for custom installs (packaging)&lt;/li&gt;
&lt;li&gt;Non blocking error notifications.&lt;/li&gt;
&lt;li&gt;Add error notifications for Imgur uploads.&lt;/li&gt;
&lt;li&gt;Minor Fixes&lt;/li&gt;
&lt;/ul&gt;
&lt;h2&gt;Extras&lt;/h2&gt;
&lt;ul&gt;
&lt;li&gt;Huge general code refactor.&lt;/li&gt;
&lt;li&gt;Dbus API refactor.&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.2.1</id>
    <updated>2017-07-04T13:13:12Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.2.1"/>
    <title>v0.2.1</title>
    <content type="html">&lt;ul&gt;
&lt;li&gt;Fix inability to change active buttons in non english translated versions of the software due to a wrong identification of the type of the buttons.&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.2.0</id>
    <updated>2017-06-19T20:27:24Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.2.0"/>
    <title>v0.2.0</title>
    <content type="html">&lt;ul&gt;
&lt;li&gt;Better selection of buttons for colour edition.&lt;/li&gt;
&lt;li&gt;Independence of full screen captures not affecting the graphical captures.&lt;/li&gt;
&lt;li&gt;Desktop entry for a more convenient desktop integration.&lt;/li&gt;
&lt;/ul&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
  <entry>
    <id>tag:github.com,2008:Repository/90902132/v0.1.0</id>
    <updated>2017-07-02T22:55:37Z</updated>
    <link rel="alternate" type="text/html" href="https://github.com/lupoDharkael/flameshot/releases/tag/v0.1.0"/>
    <title>v0.1.0</title>
    <content type="html">&lt;p&gt;First public release.&lt;/p&gt;</content>
    <author>
      <name>lupoDharkael</name>
    </author>
    <media:thumbnail height="30" width="30" url="https://avatars3.githubusercontent.com/u/14951430?s=60&amp;v=4"/>
  </entry>
</feed>
