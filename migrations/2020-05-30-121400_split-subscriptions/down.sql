create table articles_tmp (
    id text primary key,
    title text not null,
    url text not null,
    author text not null,
    content text not null,
    article_date text not null,
    is_read integer not null default 0,
    parent text not null,
    foreign key(parent) references subscriptions(url)
)

INSERT INTO articles_tmp (id, title, url, author, content, article_date, is_read, parent)
       SELECT id, title, url, author, content, article_date, is_read, subscription_data_id FROM articles;
DROP TABLE articles;
ALTER TABLE articles_tmp RENAME TO articles;

ALTER TABLE subscriptions ADD title text;
ALTER TABLE subscriptions ADD description text;

DROP TABLE subscriptions_data;
