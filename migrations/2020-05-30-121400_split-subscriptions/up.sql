CREATE TABLE subscriptions_data (
    url text not null primary key,
    title text,
    description text
);

INSERT INTO subscriptions_data (url, title, description)
       SELECT url, title, description FROM subscriptions;

-- rename parent to subscription_data_id and change foreign key
create table articles_tmp (
    id text primary key,
    title text not null,
    url text not null,
    author text not null,
    content text not null,
    article_date text not null,
    is_read integer not null default 0,
    subscription_data_id text not null,
    foreign key(subscription_data_id) references subscriptions_data(url)
);

INSERT INTO articles_tmp (id, title, url, author, content, article_date, is_read, subscription_data_id)
       SELECT id, title, url, author, content, article_date, is_read, parent FROM articles;
DROP TABLE articles;
ALTER TABLE articles_tmp RENAME TO articles;

-- drop title and description
CREATE TABLE subscriptions_tmp (
    url text not null primary key,
    subscription_order integer not null unique,
    proxy text
);

INSERT INTO subscriptions_tmp (url, subscription_order)
       SELECT url, subscription_order FROM subscriptions;
DROP TABLE subscriptions;
ALTER TABLE subscriptions_tmp RENAME TO subscriptions;
