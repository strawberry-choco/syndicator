CREATE TABLE subscriptions (
    url text not null primary key,
    title text,
    description text,
    subscription_order integer not null unique
);

create table articles (
    id text primary key,
    title text not null,
    url text not null,
    author text not null,
    content text not null,
    article_date text not null,
    is_read integer not null default 0,
    parent text not null,
    foreign key(parent) references subscriptions(url)
)
